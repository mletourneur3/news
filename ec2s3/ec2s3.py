from boto.s3.connection import S3Connection
from boto.s3.key import Key


def save2s3(filename, path_to_file, path_to_dest):
    # Path to file in local with extension
    filepath = path_to_file + filename
    # Handles connection to service
    conn = S3Connection()
    # If bucket already exists in S3, get it
    # bucket_url = 'https://s3.console.aws.amazon.com/s3/buckets/markus0dev/'
    # in datanews/
    bucket_name = 'markus0dev'
    bucket_handle = conn.get_bucket(bucket_name)
    # Create a key object to handle operations in bucket
    key_bucket = Key(bucket_handle)
    # Define the name of the file that will be stored in S3
    S3_filename = 'datanews/' + path_to_dest + filename
    # Create the file location in S3
    key_bucket.key = S3_filename
    # Store the content of your local file at the define location in S3
    key_bucket.set_contents_from_filename(filepath)
    # Set READ access to public
    bucket_handle.set_acl('public-read', key_bucket.key)