# -*- coding: utf-8 -*-
import os
import numpy as np
import pandas as pd
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import time


class Plotter():

    def _save2disk_embeddings(self, model_id, X_trans, score_kl_diver, path_output):
        """
        Save a matrix of reduction (2 dimensions) and its KL score to 
        local disk.
      
        Parameters
        ----------
        model_id: int
                ID for the underlying t-SNE model
        X_trans: numpy.ndarray
                Matrix of reduction for a t-SNE model
        score_kl_diver: float
                K-L divergence of the underlying t-SNE model
        path_output: str
                Path to output file on disk
                
        """
        df = pd.DataFrame([X_trans[:,0], X_trans[:,1]])
        
        filename = path_output + 'tsne_model' + str(model_id) + '_Xtran.csv'
        f = open(filename, 'a') # Open file as append mode
        f.write('KL divergence = %.8f\n' %score_kl_diver)
        df.to_csv(f, header = False)
        f.close()
    
    def _get_tsne_best_model(self, X, dim_red_space=2,
                             learning_rate_val=250, perplexity_val=50,
                             nb_iterations=1, op_save_path=None):
        """
        Performs the t-SNE algorithm multiple times on the dataset to return
        the model (data reduction) with the lowest KL score

        Non-convexity of the cost function leads to high variability in KL
        scores over several computation of t-SNE. Thus, we extract the best
        model out of OUTER_ITERATIONS iterations of the algorithm.

        t-SNE is performed using the TSNE function from sklearn.manifold
        package TSNE parameters are kept constant through iterations
        Barnes-Hut t-SNE is the default algorithm implemented in TSNE

        Parameters
        ----------
        X: numpy.ndarray
                Matrix of features X
        dim_red_space: int (optional, default 2)
                Dimension of the reduced space
                WARNING: only 2D reduction available
        learning_rate_val: float (optional, default 250)
                Learning rate of the t-SNE algorithm, parameter of TSNE function
                Advised range: [100, 1000]
        perplexity_val: float (optional, default 50)
                Perplexity of the t-SNE algorithm, parameter of TSNE function
                Advised range: [5, 50]
        nb_iterations: int (optional, default 1)
                Number of iterations of the t-SNE algorithm
        op_save_path: str (default, None)
                Path to save the generated t-SNE models
                If None, models are not stored on disk
                Option only available when Grid Search is disable

        Returns
        -------
        best_model: TSNE
                Model (TSNE class instance) that had the lowest KL score over
                iterations
                Attributes are kl_divergence_ (: float) that contains the KL score of
                the model, and embedding_ (: numpy.ndarray) the representation of data
                in the reduced space
        """
        best_score_kl_diver = np.Inf
        best_model = None

        for ind_iter in range(nb_iterations):
            print("Iteration #%d of TSNE.\n" % (ind_iter))
            # Default model parameters of TSNE are n_components=2, perplexity=30.0,
            # early_exaggeration=4.0, learning_rate=1000.0, n_iter=1000,
            # n_iter_without_progress=30, min_grad_norm=1e-07, metric='euclidean',
            # init='random', verbose=0, random_state=None, method='barnes_hut',
            # angle=0.5
            current_model = TSNE(n_components=dim_red_space,
                                 perplexity=perplexity_val,
                                 early_exaggeration=4.0,
                                 learning_rate=learning_rate_val, n_iter=300,
                                 n_iter_without_progress=30, min_grad_norm=1e-07,
                                 metric='euclidean', init='random', verbose=0,
                                 random_state=None, method='barnes_hut', angle=0.5)

            # Apply the t-SNE algorithm
            X_trans = current_model.fit_transform(X)
            score_kl_diver = current_model.kl_divergence_
            # Storing the model if path is provided
            if not(op_save_path is None):
                self._save2disk_embeddings(ind_iter, X_trans, score_kl_diver, op_save_path)
            
            if score_kl_diver <= best_score_kl_diver:
                best_score_kl_diver = score_kl_diver
                best_model = current_model

        return best_model

    def run_tsne_grid_search(self, X, y_labels, reference_dict=None,
                             dim_red_space=2,
                             forced_perplexity=50, forced_learning_rate=250,
                             optimi_parameter='', op_parameter_grid=[],
                             op_plot=True, op_print=True,
                             nb_outer_iterations=1, op_save_path=None):
        """
        Performs a grid search to find the t-SNE data representation that
        displays the lowest KL score over the parameter mesh/grid

        WARNING: currently only 1-dimensional grid search is available
        Only two TSNE parameters can be optimised: perplexity and learning rate

        Parameters
        ----------
        X: numpy.ndarray
                Array of features
        y_labels: numpy.array
                Vector of labels strictly associated to X
        reference_dict: str dict
                String dictionary that relates Test identifiers to their full name
                (str)
        dim_red_space: int  (optional, default 2)
                Dimension of the reduced space
                WARNING: only 2D reduction available
        forced_perplexity: float (optional, default 5)
                Perplexity of the t-SNE algorithm, parameter of TSNE function
                Advised range: [5, 50]
        forced_learning_rate: float (optional, default 250)
                Learning rate of the t-SNE algorithm, parameter of TSNE function
                Advised range: [100, 1000]
        optimi_parameter: str (optional, default '')
                Parameter that is tuned during optimisation process/grid search
                Use either None, 'perplexity' or 'learning_rate'
        op_parameter_grid: numpy.array (optional, default [])
                Array of the values that can be taken by the optimi_parameter
                It defines the grid/mesh for the grid search.
                ex: np.linspace(5, 50, 20, endpoint=True)
        op_plot: bool (optional, default True)
                Option for scatter plotting the 2D best representation
        op_print: bool (optional, default True)
                Option for printing the resulting best KL score
        nb_outer_iterations: int (optional, default 1)
                Number of iterations of the t-SNE algorithm in the get_best_tsne
                function
        op_save_path: str (default, None)
                Path to save the generated t-SNE models
                If None, models are not stored on disk
                Option only available when Grid Search is disable

        Returns
        -------
        result_list: list of dict
                List of dict entries
                Format of dict: {'kl_score': score_kl_diver, 'perplexity': ele}
                Each dict entry contains the parameters of the t-SNE, and the
                resulting
                KL score computed with the function get_best_tsne
        """
        start_time = time.time()

        if ((optimi_parameter != '')
                & (op_parameter_grid == [])):
            result_list = []
            print('Input Error: op_parameter_grid must be assigned in '
                  + 'run_tsne_grid_search.')
            return result_list

        if ((optimi_parameter == '')
                & (op_parameter_grid == [])):
            # No grid search required
            best_model = self._get_tsne_best_model(X,
                                                   dim_red_space=dim_red_space,
                                                   perplexity_val=forced_perplexity,
                                                   learning_rate_val=forced_learning_rate,
                                                   nb_iterations=nb_outer_iterations,
                                                   op_save_path=op_save_path)
            score_kl_diver = best_model.kl_divergence_
            X_trans = best_model.embedding_

            best_parameter_set = {'Grid search': False,
                                  'KL score': score_kl_diver,
                                  'Perplexity': forced_perplexity,
                                  'Learning rate': forced_learning_rate,
                                  'embeddings': X_trans}
            result_list = [best_parameter_set]

        else:
            # Multi-dimensional grid search
            # 1-D search only currently supported
            nb_dim_grid = 1

            # Creation of the result list
            result_list = []
            best_kl_score = np.inf
            best_model = None
            best_parameter_set = None

            # Assuming only 1 optimisation parameter
            # Either 'perplexity' or 'learning_rate'
            if nb_dim_grid == 1:

                grid = op_parameter_grid

                for ele in grid:

                    if optimi_parameter == 'perplexity':
                        model = self._get_tsne_best_model(X,
                                                          dim_red_space=dim_red_space,
                                                          perplexity_val=ele,
                                                          nb_iterations=nb_outer_iterations,
                                                          op_save_path=None)

                    else:
                        model = self._get_tsne_best_model(X,
                                                          dim_red_space=dim_red_space,
                                                          learning_rate_val=ele,
                                                          nb_iterations=nb_outer_iterations,
                                                          op_save_path=None)

                    score_kl_diver = model.kl_divergence_
                    X_trans = model.embedding_

                    # Result list [{cost : _ , param_1 : _ ,...},...]
                    # as dict list
                    if optimi_parameter == 'perplexity':
                        dict_entry = {'Grid search': True,
                                      'KL score': score_kl_diver,
                                      'Perplexity': ele,
                                      'Learning rate': forced_learning_rate,
                                      'embeddings': X_trans}
                    else:
                        dict_entry = {'Grid search': True,
                                      'KL score': score_kl_diver,
                                      'Perplexity': forced_perplexity,
                                      'Learning rate': ele,
                                      'embeddings': X_trans}

                    result_list.append(dict_entry)

                    # Computation of the best set of parameters
                    if score_kl_diver <= best_kl_score:
                        best_model = model
                        best_kl_score = score_kl_diver
                        best_parameter_set = dict_entry

            else:
                result_list = []
                print('t-SNE aborted. Currently, t-SNE only supports 1-D grid'
                      + ' search.')

        if (op_print & (not result_list)):
            str_to_print = '\n____________Best parameter set is____________\n'
            str_to_print += best_parameter_set
            print(str_to_print)

        if (op_plot & dim_red_space <= 2 & (not result_list)):
            # Scatter plot
            y_stages = y_labels.flatten()
            df = pd.DataFrame(
                dict(x=X_trans[0::, 0], y=X_trans[0::, 1], label=y_stages[0::]))

            groups = df.groupby('label')

            fig, ax = plt.subplots()
            # Optional, just adds 5% padding to the autoscaling
            ax.margins(0.05)
            for name, group in groups:
                strname = str(name)
                ax.plot(group.x, group.y, marker='.',
                        linestyle='', ms=9, label=strname)
                plt.draw()

            ax.legend(numpoints=1, loc='upper left')
            # Get the full name of the test as a string
            plt.title('2-D representation with'
                      + '\nt-SNE reduction\n')
                      #+ str(best_parameter_set))
            plt.xlabel('1st axis, undefined unit')
            plt.ylabel('2nd axis, undefined unit')
            plt.show()

        print('Elapsed time:', time.time() - start_time)

        return result_list
