# README

Repository for the work on 'A Million News Headlines' dataset hosted on Kaggle [https://www.kaggle.com/therohk/million-headlines]().

### Repository

##### Directories
* /data: contains the dataset and intermediary dataframes dumps.  The file is ignored for versioning and is not in the repo. It includes one word2vec representation of 10% of the entire dataset, at [https://s3-ap-southeast-1.amazonaws.com/markus0dev/datanews/abc_text_date_frac10_w2vfeat_windex.csv]()
	 
* /deprecated: contains files to trash

* /ec2s3: contains a module to upload files on AWS S3. It was used to upload files from an AWS EC2 instance to S3 as a backup. It requires a default_config.py file (with AWS credentials) to run.

* /google model: contains the pre-trained word2vec model from [https://code.google.com/archive/p/word2vec/](). The file is ignored for versioning and is not in the repo. It is available on  S3 at [https://s3-ap-southeast-1.amazonaws.com/markus0dev/GoogleNews-vectors-negative300.bin]().

* /lib: contains the t-SNE module, the intermediary models and the documentation.

* /module requirements: contains the package requirements for the EC2 conda environment, and for a virtualenv-python environment

* /report: contains the notebook for the report and the attached images. 

##### Files
* README.md: this document. It describes the repository

* Notebooks *.pynb: 
	- eda.ipynb
	- export\_to\_s3\_bucket.ipynb
	- get pre-trained word2vec.ipynb
	- major_topics.ipynb
	- word2vec\_similarity\_to\_headline.ipynb
	- word2vec\_topics\_labelling.ipynb


### Environment
We used python3.6 and jupyter notebooks for this work.
Other python modules are listed in /module requirements.
Main ones are

	- pandas
	- numpy
	- matplotlib
	- gensim
	- scikit-learn

We also used some AWS EC2 instance to perform some computation tasks.

### Notebooks

Notebooks story goes quasi-linearly in the following order
	
	1 eda.ipynb
	2 major_topics.ipynb
	3 get pre-trained word2vec.ipynb
	4 word2vec_topics_labelling.ipynb
	5 word2vec_similarity_to_headline.ipynb

 
